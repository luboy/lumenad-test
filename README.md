# LumenAd interview technical assignment

## Setup

##### 1. Clone and install dependencies:

```bash
$ git clone git@gitlab.com:luboy/lumenad-test.git
$ composer install
```

##### 2. To start up local dev environment run:

```bash
$ docker-compose up -d
```

This sets up `Nginx`, `PHP` and `PostgreSQL` containers as well as `adminer` container as a simple DB tool.

Application is exposing port `:8860`, if other docker container is using same port, please edit `docker-compose.yml` file.

DB can be accessed via Adminer tool on `localhost:8084` with `lumen` as login as password, 
or via connection string `pgsql://lumen:lumen@lumenad-db:5432/lumen` from other tool/apps.

##### 3. Run DB Migrations

Exec into the application container:

```bash
$ docker exec -ti lumenad-php /bin/sh
```

And tun migration command:

```bash
$ bin/console d:m:m
```

## API endpoints

#### Import Campaigns (SampleDataFile1.csv)

```bash
POST http://localhost:8860/api/v1/campaign

example:

curl --location --request POST 'http://localhost:8860/api/v1/campaign' \
--form 'file=@/path/toSampleFiles/SampleDataFile1.csv'
```

Response example: 

```bash
{
    "processedCount": 3
}
```

#### Import Campaign Performance Records (SampleDataFile2.csv)

```bash
POST http://localhost:8860/api/v1/performance-record

example:

curl --location --request POST 'http://localhost:8860/api/v1/performance-record' \
--form 'file=@/path/to/SampleFiles/SampleDataFile2.csv'
```

Response example: 

```bash
{
    "processedCount": 19,
    "unImportedCount": 6,
    "data": [
        {
            "campaignId": "88nv",
            "date": "01-02-2020",
            "event": "click",
            "value": 23
        },
        {
            "campaignId": "88nv",
            "date": "02-02-2020",
            "event": "click",
            "value": 235
        },
        {
            "campaignId": "88nv",
            "date": "03-02-2020",
            "event": "click",
            "value": 53
        },
        {
            "campaignId": "88nv",
            "date": "01-02-2020",
            "event": "conversion",
            "value": 10
        },
        {
            "campaignId": "88nv",
            "date": "02-02-2020",
            "event": "conversion",
            "value": 81
        },
        {
            "campaignId": "88nv",
            "date": "03-02-2020",
            "event": "conversion",
            "value": 0
        }
    ]
}
```

#### Campaign List

```bash
GET http://localhost:8860/api/v1/campaign
```

Example response:

```bash
{
    "data": [
        {
            "campaignId": "23d2",
            "name": "First Entry"
        },
        {
            "campaignId": "63ds",
            "name": "Another Record"
        },
        {
            "campaignId": "32nm",
            "name": "Yet Another Record"
        }
    ]
}
```

#### Campaign Detail

```bash
GET http://localhost:8860/api/v1/campaign/{campaignId}
```

Example response for `http://localhost:8860/api/v1/campaign/23d2`:

```bash
{
    "data": {
        "campaignId": "23d2",
        "name": "First Entry",
        "records": [
            {
                "date": "01-02-2020",
                "event": "click",
                "value": 8
            },
            {
                "date": "02-02-2020",
                "event": "click",
                "value": 1
            },
            {
                "date": "03-02-2020",
                "event": "click",
                "value": 18
            },
            {
                "date": "01-02-2020",
                "event": "conversion",
                "value": 1
            },
            {
                "date": "02-02-2020",
                "event": "conversion",
                "value": 0
            },
            {
                "date": "03-02-2020",
                "event": "conversion",
                "value": 8
            },
            {
                "date": "03-02-2020",
                "event": "conversation",
                "value": 2
            }
        ]
    }
}
```

#### Unimported Campaigns List

```bash
GET http://localhost:8860/api/v1/unimported-campaign
```

Example response:

```bash
{
    "count": 1,
    "data": [
        {
            "campaignId": "88nv"
        }
    ]
}
```

