<?php declare(strict_types=1);

namespace App\Tests\Smoke;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class ApplicationAvailabilitySmokeTest extends WebTestCase
{
    /**
     * @dataProvider urlProvider
     * @param string $url
     * @return void
     */
    public function testAuthPageIsSuccessful(string $url): void
    {
        $authClient = self::createClient();
        $authClient->request(Request::METHOD_GET, $url);
        $this->assertTrue($authClient->getResponse()->isSuccessful());
    }

    /**
     * @return array<array<string>>
     */
    public function urlProvider(): array
    {
        return [
            ['/healthz'],
        ];
    }
}
