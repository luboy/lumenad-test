<?php declare(strict_types=1);

namespace App\Tests\Unit;

use App\Domain\AggregateRoot\Campaign;
use App\Domain\AggregateRoot\Id\CampaignId;
use App\Domain\Entity\CampaignPerformanceRecord;
use App\Domain\Event\Campaign\CampaignWasCreatedEvent;
use App\Domain\Event\Campaign\CampaignWasRenamedEvent;
use App\Domain\Event\Campaign\PerformanceRecordWasAddedForCampaignEvent;
use App\Domain\Event\Campaign\PerformanceRecordWasUpdatedEvent;
use App\Infrastructure\EventStore\EventStream;
use PHPUnit\Framework\TestCase;

class CampaignTest extends TestCase
{
    /**
     * @test
     */
    public function it_can_be_created(): void
    {
        $campaignId = new CampaignId('testCampaign');
        $campaignName = 'Campaign Test';

        // prepared aggregate root
        $campaign = Campaign::create($campaignId, $campaignName);

        $this->assertEquals(2, \count($campaign->getRecordedEvents()));

        $this->assertInstanceOf(CampaignWasCreatedEvent::class, $campaign->getRecordedEvents()[0]);
        $this->assertInstanceOf(CampaignWasRenamedEvent::class, $campaign->getRecordedEvents()[1]);

        $this->assertEquals($campaignId, $campaign->getAggregateRootId());
        $this->assertEquals($campaignName, $campaign->getName());
    }

    /**
     * @test
     */
    public function it_can_be_renamed()
    {
        $campaignId = new CampaignId('testCampaign');
        $campaignName = 'Campaign Test';

        $given = [
            new CampaignWasCreatedEvent($campaignId),
            new CampaignWasRenamedEvent($campaignId, $campaignName),
        ];

        /** @var Campaign $campaign */
        $campaign = Campaign::reconstituteFromEventStream($campaignId, new EventStream($given));

        $newName = 'Renamed Campaign';

        $campaign->rename($newName);

        $this->assertEquals(1, \count($campaign->getRecordedEvents()));
        $this->assertInstanceOf(CampaignWasRenamedEvent::class, $campaign->getRecordedEvents()[0]);
        $this->assertEquals($newName,  $campaign->getName());
    }

    /**
     * @test
     */
    public function it_cannot_be_renamed_to_same_name()
    {
        $campaignId = new CampaignId('testCampaign');
        $campaignName = 'Campaign Test';

        $given = [
            new CampaignWasCreatedEvent($campaignId),
            new CampaignWasRenamedEvent($campaignId, $campaignName),
        ];

        /** @var Campaign $campaign */
        $campaign = Campaign::reconstituteFromEventStream($campaignId, new EventStream($given));

        $campaign->rename($campaignName);

        $this->assertEquals(0, \count($campaign->getRecordedEvents()));
        $this->assertEquals($campaignName,  $campaign->getName());
    }

    /**
     * @test
     */
    public function it_can_add_non_existent_performance_record(): void
    {
        $campaignId = new CampaignId('testCampaign');
        $campaignName = 'Campaign Test';

        $given = [
            new CampaignWasCreatedEvent($campaignId),
            new CampaignWasRenamedEvent($campaignId, $campaignName),
        ];

        /** @var Campaign $campaign */
        $campaign = Campaign::reconstituteFromEventStream($campaignId, new EventStream($given));

        $performanceRecord = CampaignPerformanceRecord::create(new \DateTimeImmutable(), 'click', 10);

        $campaign->addCampaignPerformanceRecord($performanceRecord);

        $this->assertEquals(1, \count($campaign->getRecordedEvents()));
        $this->assertInstanceOf(PerformanceRecordWasAddedForCampaignEvent::class, $campaign->getRecordedEvents()[0]);
    }

    /**
     * @test
     */
    public function it_will_not_add_same_existent_performance_record(): void
    {
        $campaignId = new CampaignId('testCampaign');
        $campaignName = 'Campaign Test';

        $performanceRecord = CampaignPerformanceRecord::create(new \DateTimeImmutable(), 'click', 10);

        $given = [
            new CampaignWasCreatedEvent($campaignId),
            new CampaignWasRenamedEvent($campaignId, $campaignName),
            new PerformanceRecordWasAddedForCampaignEvent(
                $campaignId,
                $performanceRecord->getDate(),
                $performanceRecord->getEventType(),
                $performanceRecord->getHits(),
            ),
        ];

        /** @var Campaign $campaign */
        $campaign = Campaign::reconstituteFromEventStream($campaignId, new EventStream($given));

        $campaign->addCampaignPerformanceRecord($performanceRecord);

        $this->assertEquals(0, \count($campaign->getRecordedEvents()));
    }

    /**
     * @test
     */
    public function it_can_update_exiting_performance_record_with_new_value(): void
    {
        $campaignId = new CampaignId('testCampaign');
        $campaignName = 'Campaign Test';

        $performanceRecord = CampaignPerformanceRecord::create(new \DateTimeImmutable(), 'click', 10);

        $given = [
            new CampaignWasCreatedEvent($campaignId),
            new CampaignWasRenamedEvent($campaignId, $campaignName),
            new PerformanceRecordWasAddedForCampaignEvent(
                $campaignId,
                $performanceRecord->getDate(),
                $performanceRecord->getEventType(),
                15,
            ),
        ];

        /** @var Campaign $campaign */
        $campaign = Campaign::reconstituteFromEventStream($campaignId, new EventStream($given));

        $campaign->addCampaignPerformanceRecord($performanceRecord);

        $this->assertEquals(1, \count($campaign->getRecordedEvents()));
        $this->assertInstanceOf(PerformanceRecordWasUpdatedEvent::class, $campaign->getRecordedEvents()[0]);

        $updatedPerformanceRecord = $campaign->getPerformanceRecord(
            $performanceRecord->getDate(),
            $performanceRecord->getEventType(),
        );

        $this->assertEquals(10, $updatedPerformanceRecord->getHits());
    }
}
