ARG PHP_VERSION=7.4.6
# default build for prod
ARG APP_ENV=prod


FROM php:${PHP_VERSION}-fpm-alpine AS builder

RUN apk add --no-cache autoconf g++ make oniguruma-dev libxml2-dev icu-dev libpng-dev postgresql-dev
RUN pecl install -o -f redis
RUN pecl install timezonedb
RUN docker-php-ext-install mysqli pdo pdo_pgsql exif mbstring xml intl opcache gd

FROM php:${PHP_VERSION}-fpm-alpine

LABEL maintainer="Vojtech Lubojacky<vojtech.lubojacky@gmail.com>"
EXPOSE 80
STOPSIGNAL SIGTERM
WORKDIR /var/www/html

COPY --from=builder /usr/local/lib/php/extensions/ /usr/local/lib/php/extensions/

COPY ./docker/php/php.ini /usr/local/etc/php/php.ini
COPY ./docker/php/php-cli.ini /usr/local/etc/php/php-cli.ini
COPY ./docker/php/opcache.ini /usr/local/etc/php/conf.d/opcache.ini
COPY ./docker/nginx/default /etc/nginx/conf.d/default.conf
COPY ./docker/nginx/nginx.conf /etc/nginx/nginx.conf
COPY ./docker/php/docker-entrypoint.sh /usr/local/bin/docker-entrypoint

RUN apk add --no-cache supervisor nginx libxml2 icu libpng gnu-libiconv postgresql \
    && docker-php-ext-enable redis timezonedb mysqli pdo pdo_pgsql exif mbstring xml intl opcache gd \
    && chmod +x /usr/local/bin/docker-entrypoint

ENV LD_PRELOAD /usr/lib/preloadable_libiconv.so php

ENTRYPOINT ["docker-entrypoint"]
CMD ["nginx", "-g", "daemon on;"]
CMD ["php-fpm"]