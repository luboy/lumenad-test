#!/bin/sh
nginx -g "daemon on;"

set -e

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
	set -- php-fpm "$@"
fi

exec docker-php-entrypoint "$@"
