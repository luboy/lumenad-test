<?php declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HealthCheckController extends AbstractController
{
    /**
     * @Route("/healthz", name="healthz")
     * @return Response
     */
    public function healthz(): Response
    {
        return new Response('ok', Response::HTTP_OK);
    }
}
