<?php declare(strict_types=1);

namespace App\Controller\Rest\Version1;

use App\ReadModel\Entity\UnImportedCampaignProjection;
use App\ReadModel\Entity\UnImportedPerformanceRecord;
use App\ReadModel\Repository\UnImportedCampaignProjectionRepositoryInterface;
use App\ReadModel\Repository\UnImportedPerformanceRecordRepositoryInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;

class UnImportedController extends AbstractFOSRestController
{
    private MessageBusInterface $commandBus;

    private UnImportedCampaignProjectionRepositoryInterface $unImportedCampaignProjectionRepository;

    private UnImportedPerformanceRecordRepositoryInterface $unImportedPerformanceRecordRepository;

    public function __construct(UnImportedCampaignProjectionRepositoryInterface $unImportedCampaignProjectionRepository,
                                UnImportedPerformanceRecordRepositoryInterface $unImportedPerformanceRecordRepository,
                                MessageBusInterface $commandBus)
    {
        $this->commandBus = $commandBus;
        $this->unImportedCampaignProjectionRepository = $unImportedCampaignProjectionRepository;
        $this->unImportedPerformanceRecordRepository = $unImportedPerformanceRecordRepository;
    }

    /**
     * @Rest\View()
     * @Rest\Get("/unimported-campaign", name="unimported_campaign_list")
     * @return View
     */
    public function listUnImportedCampaigns(): View
    {
        $campaigns = $this->unImportedCampaignProjectionRepository->findAllRecords();

        $data = $campaigns->map(static function(string $key, UnImportedCampaignProjection $campaignProjection) {
            return $campaignProjection->toArray();
        });

        $result = [
            'count' => \count($data),
            'data' => $data,
        ];

        return $this->view($result, Response::HTTP_OK);
    }

    /**
     * @Rest\View()
     * @Rest\Get("/unimported-record", name="unimported_record_list")
     * @return View
     */
    public function listUnImportedRecords(): View
    {
        $records = $this->unImportedPerformanceRecordRepository->findAllRecords();

        $data = $records->map(static function (string $key, UnImportedPerformanceRecord $record) {
            return $record->toArray();
        });

        $result = [
            'count' => \count($data),
            'data' => $data,
        ];

        return $this->view($result, Response::HTTP_OK);
    }
}
