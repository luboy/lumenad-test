<?php declare(strict_types=1);

namespace App\Controller\Rest\Version1;

use App\Domain\AggregateRoot\Id\CampaignId;
use App\Domain\Command\CreateCampaignCommand;
use App\Helper\CsvHelper;
use App\ReadModel\Entity\CampaignProjection;
use App\ReadModel\Entity\PerformanceProjection;
use App\ReadModel\Repository\CampaignProjectionRepositoryInterface;
use App\ReadModel\Repository\PerformanceProjectionRepositoryInterface;
use App\Validator\CampaignImportDataValidator;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;

class CampaignController extends AbstractFOSRestController
{
    private CampaignProjectionRepositoryInterface $campaignProjectionRepository;

    private MessageBusInterface $commandBus;

    private PerformanceProjectionRepositoryInterface $performanceProjectionRepository;

    public function __construct(CampaignProjectionRepositoryInterface $campaignProjectionRepository,
                                PerformanceProjectionRepositoryInterface $performanceProjectionRepository,
                                MessageBusInterface $commandBus)
    {
        $this->campaignProjectionRepository = $campaignProjectionRepository;
        $this->performanceProjectionRepository = $performanceProjectionRepository;
        $this->commandBus = $commandBus;
    }

    /**
     * @Rest\View()
     * @Rest\Post("/campaign", name="campaign_import")
     * @param Request $request
     * @return View
     * @throws \Exception
     */
    public function importCampaigns(Request $request)
    {
        CampaignImportDataValidator::validateRequest($request);

        $filepath = $request->files->get('file')->getRealPath();
        $data = CsvHelper::getCsvContent('campaign', $filepath);

        CampaignImportDataValidator::validateCampaignData($data);

        foreach ($data as $campaignData) {
            $this->commandBus->dispatch(
                new CreateCampaignCommand(
                    new CampaignId($campaignData['campaignId']),
                    $campaignData['name'],
                ),
            );
        }

        $result = [
            'processedCount' => \count($data),
        ];

        return $this->view($result, Response::HTTP_ACCEPTED);
    }

    /**
     * @Rest\View()
     * @Rest\Get("/campaign", name="campaign_list")
     * @return View
     */
    public function listCampaigns(): View
    {
        $campaigns = $this->campaignProjectionRepository->findAllRecords();

        $data = $campaigns->map(static function(string $key, CampaignProjection $campaignProjection) {
            return $campaignProjection->toArray();
        });

        $result = [
            'data' => $data,
        ];

        return $this->view($result, Response::HTTP_OK);
    }

    /**
     * @Rest\View()
     * @Rest\Get("/campaign/{campaignId}", name="campaign_detail")
     * @param string $campaignId
     * @return View
     */
    public function campaignDetail(string $campaignId): View
    {
        $campaign = $this->campaignProjectionRepository->findById($campaignId);

        if (!$campaign) {
            throw $this->createNotFoundException('Campaign Not Found');
        }

        $records = $this->performanceProjectionRepository->findByCampaign($campaignId)->map(
            static function (string $key, PerformanceProjection $performanceProjection) {
                return $performanceProjection->toArray();
            },
        );

        $result = [
            'data' => \array_merge($campaign->toArray(), ['records' => $records]),
        ];

        return $this->view($result, Response::HTTP_OK);
    }
}
