<?php declare(strict_types=1);

namespace App\Controller\Rest\Version1;

use App\Domain\AggregateRoot\Id\CampaignId;
use App\Domain\Command\AddPerformanceRecordToCampaignCommand;
use App\Helper\CsvHelper;
use App\ReadModel\Entity\UnImportedPerformanceRecord;
use App\ReadModel\Repository\UnImportedPerformanceRecordRepositoryInterface;
use App\Validator\PerformanceRecordImportDataValidator;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;

class PerformanceRecordController extends AbstractFOSRestController
{
    private MessageBusInterface $commandBus;

    private UnImportedPerformanceRecordRepositoryInterface $unImportedPerformanceRecordRepository;

    public function __construct(UnImportedPerformanceRecordRepositoryInterface $unImportedPerformanceRecordRepository,
                                MessageBusInterface $commandBus)
    {
        $this->commandBus = $commandBus;
        $this->unImportedPerformanceRecordRepository = $unImportedPerformanceRecordRepository;
    }

    /**
     * @Rest\View()
     * @Rest\Post("/performance-record", name="record_import")
     * @param Request $request
     * @return View
     * @throws \Exception
     */
    public function importCampaigns(Request $request)
    {
        PerformanceRecordImportDataValidator::validateRequest($request);

        $data = CsvHelper::getCsvContent('performanceRecord', $request->files->get('file')->getRealPath());

        PerformanceRecordImportDataValidator::validatePerformanceRecordData($data);

        foreach ($data as $recordData) {
            $campaignId = New CampaignId($recordData['campaignId']);

            $this->commandBus->dispatch(
                new AddPerformanceRecordToCampaignCommand(
                    $campaignId,
                    $recordData['date'],
                    $recordData['eventType'],
                    $recordData['hits']),
            );
        }

        $unImportedData = $this->unImportedPerformanceRecordRepository->findAllRecords();
        $unImported = $unImportedData->map(static function(string $key, UnImportedPerformanceRecord $record) {
            return $record->toArray();
        });

        $result = [
            'processedCount' => \count($data),
            'unImportedCount' => \count($unImported),
            'data' => $unImported,
        ];

        return $this->view($result, Response::HTTP_ACCEPTED);
    }
}
