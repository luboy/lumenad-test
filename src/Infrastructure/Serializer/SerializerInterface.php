<?php declare(strict_types=1);

namespace App\Infrastructure\Serializer;

interface SerializerInterface
{
    /**
     * @param mixed $object
     * @return array
     */
    public function serialize($object): array;

    /**
     * @param array $serializedObject
     * @return mixed
     */
    public function deserialize(array $serializedObject);
}
