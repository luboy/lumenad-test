<?php declare(strict_types=1);

namespace App\Infrastructure\EventStore;

use App\Domain\Event\Interfaces\EventsourcedEventInterface;

class EventStream
{
    /** @var array<EventsourcedEventInterface>  */
    private array $events;

    public function __construct(array $events)
    {
        $this->events = $events;
    }

    public function getEvents(): array
    {
        return $this->events;
    }
}
