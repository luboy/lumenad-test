<?php declare(strict_types=1);

namespace App\Infrastructure\EventStore\Exceptions;

use Throwable;

class ConcurrencyException extends \Exception
{
    public function __construct(string $message = "", int $code = 0, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
