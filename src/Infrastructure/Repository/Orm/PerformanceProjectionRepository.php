<?php declare(strict_types=1);

namespace App\Infrastructure\Repository\Orm;

use App\Domain\AggregateRoot\Id\CampaignId;
use App\ReadModel\Entity\PerformanceProjection;
use App\ReadModel\Repository\PerformanceProjectionRepositoryInterface;
use DateTimeImmutable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Ds\Map;

/**
 * @method PerformanceProjection|null find($id, $lockMode = null, $lockVersion = null)
 * @method PerformanceProjection|null findOneBy(array $criteria, array $orderBy = null)
 * @method PerformanceProjection[]    findAll()
 * @method PerformanceProjection[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PerformanceProjectionRepository extends ServiceEntityRepository implements PerformanceProjectionRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PerformanceProjection::class);
    }

    public function save(PerformanceProjection $performanceProjection): void
    {
        $this->getEntityManager()->persist($performanceProjection);
        $this->getEntityManager()->flush();
    }

    public function findByCampaignDateType(CampaignId $campaignId,
                                           DateTimeImmutable $date,
                                           string $eventType): ?PerformanceProjection
    {
        return $this->findOneBy([
            'campaignId' => $campaignId,
            'date' => $date,
            'eventType' => $eventType,
        ]);
    }

    public function findByCampaign(string $campaignId): Map
    {
        $projections =  $this->findBy([
            'campaignId' => $campaignId,
        ]);

        $result = new Map();

        foreach ($projections as $projection) {
            $result->put($projection->getId(), $projection);
        }

        return $result;
    }

    public function findAllRecords(): Map
    {
        $projections = $this->findAll();
        $result = new Map();

        foreach ($projections as $projection) {
            $result->put($projection->getId(), $projection);
        }

        return $result;
    }
}
