<?php declare(strict_types=1);

namespace App\Infrastructure\Repository\Orm;

use App\ReadModel\Entity\UnImportedCampaignProjection;
use App\ReadModel\Repository\UnImportedCampaignProjectionRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Ds\Map;

/**
 * @method UnImportedCampaignProjection|null find($id, $lockMode = null, $lockVersion = null)
 * @method UnImportedCampaignProjection|null findOneBy(array $criteria, array $orderBy = null)
 * @method UnImportedCampaignProjection[]    findAll()
 * @method UnImportedCampaignProjection[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UnImportedCampaignProjectionRepository extends ServiceEntityRepository implements UnImportedCampaignProjectionRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UnImportedCampaignProjection::class);
    }

    public function save(UnImportedCampaignProjection $unImportedCampaignProjection): void
    {
        $this->getEntityManager()->persist($unImportedCampaignProjection);
        $this->getEntityManager()->flush();
    }

    public function findById(string $id): ?UnImportedCampaignProjection
    {
        return $this->find($id);
    }

    public function findAllRecords(): Map
    {
        $projections = $this->findAll();
        $result = new Map();

        foreach ($projections as $projection) {
            $result->put($projection->getId(), $projection);
        }

        return $result;
    }

    public function delete(UnImportedCampaignProjection $unImportedCampaignProjection): void
    {
        $this->getEntityManager()->remove($unImportedCampaignProjection);
        $this->getEntityManager()->flush();
    }
}
