<?php declare(strict_types=1);

namespace App\Infrastructure\Repository\Orm;

use App\ReadModel\Entity\CampaignProjection;
use App\ReadModel\Repository\CampaignProjectionRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Ds\Map;

/**
 * @method CampaignProjection|null find($id, $lockMode = null, $lockVersion = null)
 * @method CampaignProjection|null findOneBy(array $criteria, array $orderBy = null)
 * @method CampaignProjection[]    findAll()
 * @method CampaignProjection[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CampaignProjectionRepository extends ServiceEntityRepository implements CampaignProjectionRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CampaignProjection::class);
    }

    public function save(CampaignProjection $campaignProjection): void
    {
        $this->getEntityManager()->persist($campaignProjection);
        $this->getEntityManager()->flush();
    }

    public function findById(string $id): ?CampaignProjection
    {
        return $this->find($id);
    }

    public function findAllRecords(): Map
    {
        $projections = $this->findAll();
        $result = new Map();

        foreach ($projections as $projection) {
            $result->put($projection->getId(), $projection);
        }

        return $result;
    }
}
