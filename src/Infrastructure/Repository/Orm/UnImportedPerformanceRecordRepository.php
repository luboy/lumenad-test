<?php declare(strict_types=1);

namespace App\Infrastructure\Repository\Orm;

use App\ReadModel\Entity\UnImportedPerformanceRecord;
use App\ReadModel\Repository\UnImportedPerformanceRecordRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Ds\Map;

/**
 * @method UnImportedPerformanceRecord|null find($id, $lockMode = null, $lockVersion = null)
 * @method UnImportedPerformanceRecord|null findOneBy(array $criteria, array $orderBy = null)
 * @method UnImportedPerformanceRecord[]    findAll()
 * @method UnImportedPerformanceRecord[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UnImportedPerformanceRecordRepository extends ServiceEntityRepository implements UnImportedPerformanceRecordRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UnImportedPerformanceRecord::class);
    }

    public function save(UnImportedPerformanceRecord $unImportedPerformanceRecord): void
    {
        $this->getEntityManager()->persist($unImportedPerformanceRecord);
        $this->getEntityManager()->flush();
    }

    public function findAllRecords(): Map
    {
        $records = $this->findAll();
        $result = new Map();

        foreach ($records as $record) {
            $result->put($record->getId(), $record);
        }

        return $result;
    }

    public function delete(UnImportedPerformanceRecord $unImportedPerformanceRecord): void
    {
        $this->getEntityManager()->remove($unImportedPerformanceRecord);
        $this->getEntityManager()->flush();
    }

    public function findByCampaignDateType(string $campaignId,
                                           \DateTimeImmutable $date,
                                           string $eventType): ?UnImportedPerformanceRecord
    {
        return $this->findOneBy([
            'campaignId' => $campaignId,
            'date' => $date,
            'eventType' => $eventType,
        ]);
    }

    public function findByCampaign(string $campaignId): Map
    {
        $records = $this->findBy([
            'campaignId' => $campaignId,
        ]);
        $result = new Map();

        foreach ($records as $record) {
            $result->put($record->getId(), $record);
        }

        return $result;
    }
}
