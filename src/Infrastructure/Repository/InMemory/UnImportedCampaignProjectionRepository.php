<?php declare(strict_types=1);

namespace App\Infrastructure\Repository\InMemory;

use App\ReadModel\Entity\UnImportedCampaignProjection;
use App\ReadModel\Repository\UnImportedCampaignProjectionRepositoryInterface;
use Ds\Map;

class UnImportedCampaignProjectionRepository implements UnImportedCampaignProjectionRepositoryInterface
{
    /** @var Map<string,UnImportedCampaignProjection> */
    private Map $campaigns;

    public function __construct()
    {
        $this->campaigns = new Map();
    }

    public function save(UnImportedCampaignProjection $unImportedUnImportedCampaignProjection): void
    {
        $this->campaigns->put(
            $unImportedUnImportedCampaignProjection->getId(),
            $unImportedUnImportedCampaignProjection,
        );
    }

    /**
     * @return Map<string,UnImportedCampaignProjection>
     */
    public function findAllRecords(): Map
    {
        return $this->campaigns;
    }

    public function findById(string $id): ?UnImportedCampaignProjection
    {
        if (!$this->campaigns->hasKey($id)) {
            return null;
        }

        return $this->campaigns->get($id);
    }

    public function delete(UnImportedCampaignProjection $unImportedCampaignProjection): void
    {
        if (!$this->campaigns->hasKey($unImportedCampaignProjection->getId())) {
            return;
        }

        $this->campaigns->remove($unImportedCampaignProjection->getId());
    }
}
