<?php declare(strict_types=1);

namespace App\Infrastructure\Repository\InMemory;

use App\ReadModel\Entity\UnImportedPerformanceRecord;
use App\ReadModel\Repository\UnImportedPerformanceRecordRepositoryInterface;
use Ds\Map;

class UnImportedPerformanceRecordRepository implements UnImportedPerformanceRecordRepositoryInterface
{
    /** @var Map<string,UnImportedPerformanceRecord> */
    private Map $records;

    public function __construct()
    {
        $this->records = new Map();
    }

    public function save(UnImportedPerformanceRecord $unImportedPerformanceRecord): void
    {
        $this->records->put($this->createIdentifier($unImportedPerformanceRecord), $unImportedPerformanceRecord);
    }

    private function createIdentifier(UnImportedPerformanceRecord $unImportedPerformanceRecord): string
    {
        return \sprintf('%s-%s-%s',
            $unImportedPerformanceRecord->getCampaignId(),
            $unImportedPerformanceRecord->getDate()->format('d-m-Y'),
            $unImportedPerformanceRecord->getEventType(),
        );
    }

    /**
     * @return Map<string,UnImportedPerformanceRecord>
     */
    public function findAllRecords(): Map
    {
        return $this->records;
    }

    public function delete(UnImportedPerformanceRecord $unImportedPerformanceRecord): void
    {
        if (!$this->records->hasKey($this->createIdentifier($unImportedPerformanceRecord))) {
            return;
        }

        $this->records->remove($this->createIdentifier($unImportedPerformanceRecord));
    }

    public function findByCampaignDateType(string $campaignId, \DateTimeImmutable $date, string $eventType): ?UnImportedPerformanceRecord
    {
        $filtered = $this->records->filter(
            static function (string $key, UnImportedPerformanceRecord $record) use ($campaignId, $date, $eventType) {
                return $record->getCampaignId() === $campaignId
                    && $record->getDate()->getTimestamp() === $date->getTimestamp()
                    && $record->getEventType() === $eventType;
            },
        );

        if ($filtered->isEmpty()) {
            return null;
        }

        return $filtered->first()->value;
    }

    public function findByCampaign(string $campaignId): Map
    {
        return $this->records->filter(
            static function (string $key, UnImportedPerformanceRecord $record) use ($campaignId) {
                return $record->getCampaignId() === $campaignId;
            },
        );
    }
}
