<?php declare(strict_types=1);

namespace App\Infrastructure\Repository\InMemory;

use App\Domain\AggregateRoot\Id\CampaignId;
use App\ReadModel\Entity\PerformanceProjection;
use App\ReadModel\Repository\PerformanceProjectionRepositoryInterface;
use DateTimeImmutable;
use Ds\Map;

class PerformanceProjectionRepository implements PerformanceProjectionRepositoryInterface
{
    /** @var Map<string,PerformanceProjection> */
    private Map $performanceProjections;

    public function __construct()
    {
        $this->performanceProjections = new Map();
    }

    private function createIdentifier(PerformanceProjection $performanceProjection): string
    {
        return \sprintf('%s-%s-%s',
            $performanceProjection->getCampaignId(),
            $performanceProjection->getDate()->format('d-m-Y'),
            $performanceProjection->getEventType(),
        );
    }

    public function save(PerformanceProjection $performanceProjection): void
    {

        $this->performanceProjections->put(
            $this->createIdentifier($performanceProjection),
            $performanceProjection,
        );
    }

    /**
     * @return Map<string,PerformanceProjection>
     */
    public function findAllRecords(): Map
    {
        return $this->performanceProjections;
    }

    public function findByCampaignDateType(CampaignId $campaignId, DateTimeImmutable $date, string $eventType): ?PerformanceProjection
    {
        $filtered = $this->performanceProjections->filter(
            static function(string $key, PerformanceProjection $item) use ($campaignId, $date, $eventType)  {
                return $item->getCampaignId() === $campaignId->getId()
                    && $item->getDate()->getTimestamp() === $date->getTimestamp()
                    && $item->getEventType() === $eventType;
            });

        if ($filtered->isEmpty()) {
            return null;
        }

        return $filtered->first()->value;
    }

    public function findByCampaign(string $campaignId): Map
    {
        return $this->performanceProjections->filter(
            static function (string $key, PerformanceProjection $item) use ($campaignId) {
                return $item->getCampaignId() === $campaignId;
            });
    }
}
