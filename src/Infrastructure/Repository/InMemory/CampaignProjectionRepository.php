<?php declare(strict_types=1);

namespace App\Infrastructure\Repository\InMemory;

use App\ReadModel\Entity\CampaignProjection;
use App\ReadModel\Repository\CampaignProjectionRepositoryInterface;
use Ds\Map;

class CampaignProjectionRepository implements CampaignProjectionRepositoryInterface
{
    /** @var Map<string,CampaignProjection> */
    private Map $campaigns;

    public function __construct()
    {
        $this->campaigns = new Map();
    }

    public function save(CampaignProjection $campaignProjection): void
    {
        $this->campaigns->put($campaignProjection->getId(), $campaignProjection);
    }

    /**
     * @return Map<string,CampaignProjection>
     */
    public function findAllRecords(): Map
    {
        return $this->campaigns;
    }

    public function findById(string $id): ?CampaignProjection
    {
        if (!$this->campaigns->hasKey($id)) {
            return null;
        }

        return $this->campaigns->get($id);
    }
}
