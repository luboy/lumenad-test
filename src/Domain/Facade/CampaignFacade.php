<?php declare(strict_types=1);

namespace App\Domain\Facade;

use App\Domain\AggregateRoot\Campaign;
use App\Domain\AggregateRoot\Id\CampaignId;
use App\Domain\Entity\CampaignPerformanceRecord;
use App\Domain\Repository\CampaignRepository;
use App\Exception\NonExistingEntityException;
use App\Infrastructure\EventStore\Exceptions\ConcurrencyException;

class CampaignFacade
{
    private CampaignRepository $campaignRepository;

    public function __construct(CampaignRepository $campaignRepository)
    {
        $this->campaignRepository = $campaignRepository;
    }

    /**
     * @param CampaignId $campaignId
     * @param string $name
     * @throws ConcurrencyException
     */
    public function createCampaign(CampaignId $campaignId, string $name): void
    {
        $campaign = Campaign::create($campaignId, $name);
        $this->campaignRepository->save($campaign);
    }

    /**
     * @param CampaignId $campaignId
     * @return Campaign
     * @throws NonExistingEntityException
     */
    public function getCampaignById(CampaignId $campaignId): Campaign
    {
        $campaign = $this->campaignRepository->getCampaign($campaignId);

        if (!$campaign) {
            throw new NonExistingEntityException(Campaign::class, $campaignId);
        }

        return $campaign;
    }

    /**
     * @param CampaignId $campaignId
     * @param CampaignPerformanceRecord $performanceRecord
     * @throws NonExistingEntityException
     * @throws ConcurrencyException
     */
    public function addPerformanceRecordToCampaign(CampaignId $campaignId, CampaignPerformanceRecord $performanceRecord): void
    {
        $campaign = $this->getCampaignById($campaignId);
        $campaign->addCampaignPerformanceRecord($performanceRecord);

        $this->campaignRepository->save($campaign);
    }

    public function updateCampaign(CampaignId $campaignId, string $name): void
    {
        $campaign = $this->getCampaignById($campaignId);
        $campaign->rename($name);
    }
}
