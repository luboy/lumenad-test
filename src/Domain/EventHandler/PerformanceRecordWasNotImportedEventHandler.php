<?php declare(strict_types=1);

namespace App\Domain\EventHandler;

use App\Domain\Event\Campaign\PerformanceRecordWasNotImportedEvent;
use App\Domain\EventHandler\Interfaces\SyncEventHandlerInterface;
use App\ReadModel\Projector\CampaignProjector;

class PerformanceRecordWasNotImportedEventHandler implements SyncEventHandlerInterface
{
    private CampaignProjector $campaignProjector;

    public function __construct(CampaignProjector $campaignProjector)
    {
        $this->campaignProjector = $campaignProjector;
    }

    public function __invoke(PerformanceRecordWasNotImportedEvent $event): void
    {
        $this->campaignProjector->apply($event);
    }
}
