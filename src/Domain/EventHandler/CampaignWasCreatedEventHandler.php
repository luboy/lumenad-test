<?php declare(strict_types=1);

namespace App\Domain\EventHandler;

use App\Domain\Command\PairUnlinkedPerformanceRecordsCommand;
use App\Domain\Event\Campaign\CampaignWasCreatedEvent;
use App\Domain\EventHandler\Interfaces\SyncEventHandlerInterface;
use App\ReadModel\Projector\CampaignProjector;
use Symfony\Component\Messenger\MessageBusInterface;

class CampaignWasCreatedEventHandler implements SyncEventHandlerInterface
{
    private CampaignProjector $campaignProjector;

    private MessageBusInterface $commandBus;

    public function __construct(CampaignProjector $campaignProjector, MessageBusInterface $commandBus)
    {
        $this->campaignProjector = $campaignProjector;
        $this->commandBus = $commandBus;
    }

    public function __invoke(CampaignWasCreatedEvent $event): void
    {
        $this->campaignProjector->apply($event);

        $this->commandBus->dispatch(
            new PairUnlinkedPerformanceRecordsCommand($event->getCampaignId()),
        );
    }
}
