<?php declare(strict_types=1);

namespace App\Domain\Command;

use App\Domain\AggregateRoot\Id\CampaignId;

class AddPerformanceRecordToCampaignCommand
{
    private CampaignId $campaignId;

    private \DateTimeImmutable $date;

    private string $eventType;

    private int $hits;

    public function __construct(CampaignId $campaignId, \DateTimeImmutable $date, string $eventType, int $hits)
    {
        $this->campaignId = $campaignId;
        $this->date = $date;
        $this->eventType = $eventType;
        $this->hits = $hits;
    }

    public function getCampaignId(): CampaignId
    {
        return $this->campaignId;
    }

    public function getDate(): \DateTimeImmutable
    {
        return $this->date;
    }

    public function getEventType(): string
    {
        return $this->eventType;
    }

    public function getHits(): int
    {
        return $this->hits;
    }
}
