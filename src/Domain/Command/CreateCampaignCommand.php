<?php declare(strict_types=1);

namespace App\Domain\Command;

use App\Domain\AggregateRoot\Id\CampaignId;

class CreateCampaignCommand
{
    private CampaignId $campaignId;

    private string $name;

    public function __construct(CampaignId $campaignId, string $name)
    {
        $this->campaignId = $campaignId;
        $this->name = $name;
    }

    public function getCampaignId(): CampaignId
    {
        return $this->campaignId;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
