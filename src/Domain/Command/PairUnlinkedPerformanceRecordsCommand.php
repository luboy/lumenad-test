<?php declare(strict_types=1);

namespace App\Domain\Command;

use App\Domain\AggregateRoot\Id\CampaignId;

class PairUnlinkedPerformanceRecordsCommand
{
    private CampaignId $campaignId;

    public function __construct(CampaignId $campaignId)
    {
        $this->campaignId = $campaignId;
    }

    public function getCampaignId(): CampaignId
    {
        return $this->campaignId;
    }
}
