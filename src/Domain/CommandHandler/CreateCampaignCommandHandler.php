<?php declare(strict_types=1);

namespace App\Domain\CommandHandler;

use App\Domain\Command\CreateCampaignCommand;
use App\Domain\CommandHandler\Interfaces\SyncCommandHandlerInterface;
use App\Domain\Facade\CampaignFacade;

class CreateCampaignCommandHandler implements SyncCommandHandlerInterface
{
    private CampaignFacade $campaignFacade;

    public function __construct(CampaignFacade $campaignFacade)
    {
        $this->campaignFacade = $campaignFacade;
    }

    public function __invoke(CreateCampaignCommand $command): void
    {
        try {
            $this->campaignFacade->updateCampaign($command->getCampaignId(), $command->getName());

            return;
        } catch (\Throwable $throwable) {

        }

        $this->campaignFacade->createCampaign($command->getCampaignId(), $command->getName());
    }
}
