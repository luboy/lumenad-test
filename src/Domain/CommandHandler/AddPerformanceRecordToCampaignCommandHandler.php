<?php declare(strict_types=1);

namespace App\Domain\CommandHandler;

use App\Domain\Command\AddPerformanceRecordToCampaignCommand;
use App\Domain\CommandHandler\Interfaces\SyncCommandHandlerInterface;
use App\Domain\Entity\CampaignPerformanceRecord;
use App\Domain\Event\Campaign\PerformanceRecordWasNotImportedEvent;
use App\Domain\Facade\CampaignFacade;
use App\Exception\NonExistingEntityException;
use Symfony\Component\Messenger\MessageBusInterface;

class AddPerformanceRecordToCampaignCommandHandler implements SyncCommandHandlerInterface
{
    private CampaignFacade $campaignFacade;

    private MessageBusInterface $eventBus;

    public function __construct(CampaignFacade $campaignFacade, MessageBusInterface $eventBus)
    {
        $this->campaignFacade = $campaignFacade;
        $this->eventBus = $eventBus;
    }

    public function __invoke(AddPerformanceRecordToCampaignCommand $command): void
    {
        $performanceRecord = CampaignPerformanceRecord::create(
            $command->getDate(),
            $command->getEventType(),
            $command->getHits(),
        );

        try {
            $this->campaignFacade->addPerformanceRecordToCampaign($command->getCampaignId(), $performanceRecord);
        } catch (NonExistingEntityException $throwable) {
            $this->eventBus->dispatch(
                new PerformanceRecordWasNotImportedEvent(
                    $command->getCampaignId(),
                    $command->getDate(),
                    $command->getEventType(),
                    $command->getHits(),
                ),
            );
        }
    }
}
