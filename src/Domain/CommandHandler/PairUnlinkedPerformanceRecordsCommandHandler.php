<?php declare(strict_types=1);

namespace App\Domain\CommandHandler;

use App\Domain\Command\PairUnlinkedPerformanceRecordsCommand;
use App\Domain\CommandHandler\Interfaces\SyncCommandHandlerInterface;
use App\Domain\Entity\CampaignPerformanceRecord;
use App\Domain\Facade\CampaignFacade;
use App\ReadModel\Repository\UnImportedPerformanceRecordRepositoryInterface;

class PairUnlinkedPerformanceRecordsCommandHandler implements SyncCommandHandlerInterface
{
    private CampaignFacade $campaignFacade;

    private UnImportedPerformanceRecordRepositoryInterface $unImportedPerformanceRecordRepository;

    public function __construct(UnImportedPerformanceRecordRepositoryInterface $unImportedPerformanceRecordRepository, CampaignFacade $campaignFacade)
    {
        $this->campaignFacade = $campaignFacade;
        $this->unImportedPerformanceRecordRepository = $unImportedPerformanceRecordRepository;
    }

    public function __invoke(PairUnlinkedPerformanceRecordsCommand $command): void
    {
        $records = $this->unImportedPerformanceRecordRepository->findByCampaign($command->getCampaignId()->getId());

        foreach ($records as $record) {
            $performanceRecord = CampaignPerformanceRecord::createFromUnImported($record);
            $this->campaignFacade->addPerformanceRecordToCampaign($command->getCampaignId(), $performanceRecord);
        }
    }
}
