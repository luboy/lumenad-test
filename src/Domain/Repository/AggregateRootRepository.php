<?php declare(strict_types=1);

namespace App\Domain\Repository;

use App\Domain\AggregateRoot\AggregateRoot;
use App\Domain\AggregateRoot\Id\AggregateRootId;
use App\Infrastructure\EventStore\EventStoreInterface;
use App\Infrastructure\EventStore\EventStream;
use App\Infrastructure\EventStore\Exceptions\ConcurrencyException;

class AggregateRootRepository
{
    private EventStoreInterface $eventStore;

    public function __construct(EventStoreInterface $eventStore)
    {
        $this->eventStore = $eventStore;
    }

    protected function loadEventStream(string $uuid): ?EventStream
    {
        return $this->eventStore->load($uuid);
    }

    public function save(AggregateRoot $aggregateRoot): void
    {
        $eventStream = $this->loadEventStream($aggregateRoot->getAggregateRootId()->getId());

        $lastSavedPlayhead = -1;

        if ($eventStream) {
            $events = $eventStream->getEvents();
            $lastSavedPlayhead = \count($events) - 1;

            if ($lastSavedPlayhead + \count($aggregateRoot->getRecordedEvents()) !== $aggregateRoot->getPlayHead()) {
                throw new ConcurrencyException();
            }
        }

        $this->eventStore->append($aggregateRoot->getRecordedEvents(), $lastSavedPlayhead);

        $aggregateRoot->clearRecordedEvents();
    }

    protected function get(AggregateRootId $aggregateRootId, string $className): ?AggregateRoot
    {
        $eventStream = $this->loadEventStream($aggregateRootId->getId());

        if (!$eventStream) {
            return null;
        }

        /** @var AggregateRoot $className */

        return $className::reconstituteFromEventStream($aggregateRootId, $eventStream);
    }
}
