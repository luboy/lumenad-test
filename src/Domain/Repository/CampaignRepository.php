<?php declare(strict_types=1);

namespace App\Domain\Repository;

use App\Domain\AggregateRoot\Campaign;
use App\Domain\AggregateRoot\Id\AggregateRootId;

class CampaignRepository extends AggregateRootRepository
{
    public function getCampaign(AggregateRootId $aggregateRootId): ?Campaign
    {
        $campaign = parent::get($aggregateRootId, Campaign::class);

        if ($campaign instanceof Campaign) {
            return $campaign;
        }

        return null;
    }
}
