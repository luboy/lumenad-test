<?php declare(strict_types=1);

namespace App\Domain\AggregateRoot;

use App\Domain\AggregateRoot\Id\AggregateRootId;
use App\Domain\Event\Interfaces\EventsourcedEventInterface;
use App\Infrastructure\EventStore\EventStream;

interface AggregateRootInterface
{
    public static function reconstituteFromEventStream(AggregateRootId $aggregateRootId, EventStream $eventStream): AggregateRoot;

    public function getRecordedEvents(): array;

    public function clearRecordedEvents(): void;

    public function recordThat(EventsourcedEventInterface $event): void;

    public function apply(EventsourcedEventInterface $event): void;

    public function getApplyMethod(EventsourcedEventInterface $event): string;

    public function addRecordedEvent(EventsourcedEventInterface $recordedEvent): void;

    public function getPlayHead(): int;

    public function setPlayHead(int $playHead): void;

    public function incrementPlayHead(): void;

    public function getAggregateRootId(): AggregateRootId;
}
