<?php declare(strict_types=1);

namespace App\Domain\AggregateRoot;

use App\Domain\AggregateRoot\Id\AggregateRootId;
use App\Domain\AggregateRoot\Id\CampaignId;
use App\Domain\Entity\CampaignPerformanceRecord;
use App\Domain\Event\Campaign\CampaignWasCreatedEvent;
use App\Domain\Event\Campaign\CampaignWasRenamedEvent;
use App\Domain\Event\Campaign\PerformanceRecordWasAddedForCampaignEvent;
use App\Domain\Event\Campaign\PerformanceRecordWasUpdatedEvent;
use Ds\Map;

final class Campaign extends AggregateRoot
{
    private CampaignId $id;

    private string $name;

    /**
     * @var Map<string,CampaignPerformanceRecord>
     */
    private Map $performanceRecords;

    public function __construct(CampaignId $campaignId)
    {
        parent::__construct($campaignId);

        $this->name = '';
        $this->performanceRecords = new Map();
    }

    public static function create(CampaignId $campaignId, string $name): self
    {
        $campaign = new static($campaignId);

        $campaign->recordThat(
            new CampaignWasCreatedEvent($campaignId),
        );

        $campaign->rename($name);

        return $campaign;
    }

    // ---------- public api ----------

    public function rename(string $renameTo): void
    {
        if ($this->name === $renameTo) {
            return; // no domain change happened
        }

        $this->recordThat(
            new CampaignWasRenamedEvent($this->id, $renameTo),
        );
    }

    public function addCampaignPerformanceRecord(CampaignPerformanceRecord $record): void
    {
        if ($this->isRecorded($record)) {
            $this->updateCampaignPerformanceRecord($record);

            return;
        }

        $this->recordThat(
            new PerformanceRecordWasAddedForCampaignEvent(
                $this->id,
                $record->getDate(),
                $record->getEventType(),
                $record->getHits(),
            ),
        );
    }

    public function updateCampaignPerformanceRecord(CampaignPerformanceRecord $record): void
    {
        if ($this->isRecordedValueUnchanged($record)) {
            return;
        }

        /** @var CampaignPerformanceRecord $recorded */
        $recorded = $this->performanceRecords->get($record->hash());

        $delta = $record->getHits() - $recorded->getHits();

        $this->recordThat(
            new PerformanceRecordWasUpdatedEvent($this->id, $record->getDate(), $record->getEventType(), $delta),
        );
    }

    // ---------- domain events application ----------

    protected function applyCampaignWasCreatedEvent(CampaignWasCreatedEvent $event): void
    {
        $this->id = $event->getCampaignId();
    }

    protected function applyCampaignWasRenamedEvent(CampaignWasRenamedEvent $event): void
    {
        $this->name = $event->getName();
    }

    protected function applyPerformanceRecordWasAddedForCampaignEvent(PerformanceRecordWasAddedForCampaignEvent $event): void
    {
        $record = CampaignPerformanceRecord::create($event->getDate(), $event->getEventType(), $event->getHits());

        $this->performanceRecords->put($record->hash(), $record);
    }

    protected function applyPerformanceRecordWasUpdatedEvent(PerformanceRecordWasUpdatedEvent $event): void
    {
        /** @var CampaignPerformanceRecord $record */
        $record = $this->performanceRecords->get($event->getHash());

        $record->applyHitsChange($event->getDelta());

        $this->performanceRecords->put($record->hash(), $record);
    }

    // --------- domain invariants handling ----------

    private function isRecorded(CampaignPerformanceRecord $record): bool
    {
        return $this->performanceRecords->hasKey($record->hash());
    }

    private function isRecordedValueUnchanged(CampaignPerformanceRecord $record): bool
    {
        return $record->equals($this->performanceRecords->get($record->hash()));
    }

    // ---------- getters ----------

    public function getAggregateRootId(): AggregateRootId
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPerformanceRecord(\DateTimeImmutable $date, string $eventType): ?CampaignPerformanceRecord
    {
        $hash = \sprintf('%s-%s', $date->format('d-m-Y'), $eventType);

        if (!$this->performanceRecords->hasKey($hash)) {
            return null;
        }

        return $this->performanceRecords->get($hash);
    }
}
