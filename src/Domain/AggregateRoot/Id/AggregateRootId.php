<?php declare(strict_types=1);

namespace App\Domain\AggregateRoot\Id;

use Ds\Hashable;

class AggregateRootId implements Hashable
{
    protected string $id;

    public function __construct(string $id)
    {
        $this->id = (string) $id;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function __toString(): string
    {
        return $this->id;
    }

    function hash(): string
    {
        return $this->id;
    }

    /**
     * @param AggregateRootId $obj
     * @return bool
     */
    function equals($obj): bool
    {
        return $this->id === $obj->getId();
    }
}
