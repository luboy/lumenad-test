<?php declare(strict_types=1);

namespace App\Domain\AggregateRoot;

use App\Domain\AggregateRoot\Id\AggregateRootId;
use App\Domain\Event\Interfaces\EventsourcedEventInterface;
use App\Infrastructure\EventStore\EventStream;

abstract class AggregateRoot implements AggregateRootInterface
{
    // @phpstan-ignore-next-line
    public function __construct(AggregateRootId $aggregateRootId)
    {

    }

    /** @var EventsourcedEventInterface[] */
    protected array $recordedEvents = [];

    // 0-based playhead allows events[0] to have playhead 0
    protected int $playHead = -1;

    public function getRecordedEvents(): array
    {
        return $this->recordedEvents;
    }

    public function clearRecordedEvents(): void
    {
        $this->recordedEvents = [];
    }

    public function getPlayHead(): int
    {
        return $this->playHead;
    }

    public function setPlayHead(int $playHead): void
    {
        $this->playHead = $playHead;
    }

    public function incrementPlayhead(): void
    {
        ++$this->playHead;
    }

    /**
     * @throws \Exception
     */
    public function recordThat(EventsourcedEventInterface $event): void
    {
        $this->incrementPlayhead();
        $this->addRecordedEvent($event);
        $this->apply($event);
    }

    public function apply(EventsourcedEventInterface $event): void
    {
        $method = $this->getApplyMethod($event);
        $this->$method($event);
    }

    public static function reconstituteFromEventStream(AggregateRootId $aggregateRootId, EventStream $eventStream): AggregateRoot
    {
        // @phpstan-ignore-next-line
        $aggregate = new static($aggregateRootId);

        /** @var EventsourcedEventInterface $event */
        foreach ($eventStream->getEvents() as $event) {
            $aggregate->apply($event);
            $aggregate->incrementPlayHead();
        }

        return $aggregate;
    }

    public function getApplyMethod(EventsourcedEventInterface $event): string
    {
        $classNameParts = \explode('\\', \get_class($event));

        return 'apply' . \end($classNameParts);
    }

    public function addRecordedEvent(EventsourcedEventInterface $recordedEvent): void
    {
        $this->recordedEvents[] = $recordedEvent;
    }
}
