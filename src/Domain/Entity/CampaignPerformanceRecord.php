<?php declare(strict_types=1);

namespace App\Domain\Entity;

use App\ReadModel\Entity\UnImportedPerformanceRecord;
use Ds\Hashable;

class CampaignPerformanceRecord implements Hashable
{
    private \DateTimeImmutable $date;

    private string $eventType;

    private int $hits = 0;

    public function __construct(\DateTimeImmutable $date,
                                string $eventType,
                                int $hits)
    {
        $this->date = $date->setTime(0,0,0,0);
        $this->eventType = $eventType;
        $this->hits = $hits;
    }

    public static function create(\DateTimeImmutable $date,
                                  string $eventType,
                                  int $hits): self
    {
        return new self($date, $eventType, $hits);
    }

    public static function createFromUnImported(UnImportedPerformanceRecord $record): self
    {
        return new self($record->getDate(), $record->getEventType(), $record->getHits());
    }

    // ---------- public api ----------

    public function applyHitsChange(int $delta): void
    {
        $this->hits += $delta;
    }

    // ---------- getters ----------

    public function getDate(): \DateTimeImmutable
    {
        return $this->date;
    }

    public function getEventType(): string
    {
        return $this->eventType;
    }

    public function getHits(): int
    {
        return $this->hits;
    }

    public function hash(): string
    {
        return \sprintf('%s-%s', $this->date->format('d-m-Y'), $this->eventType);
    }

    /**
     * @param self $obj
     * @return bool
     */
    function equals($obj): bool
    {
        return $this->date->getTimestamp() === $obj->getDate()->getTimestamp()
        && $this->eventType === $obj->getEventType()
        && $this->hits === $obj->getHits();
    }
}
