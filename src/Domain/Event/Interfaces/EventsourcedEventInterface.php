<?php declare(strict_types=1);

namespace App\Domain\Event\Interfaces;

use App\Domain\AggregateRoot\Id\AggregateRootId;

interface EventsourcedEventInterface
{
    public function getAggregateId(): AggregateRootId;
}
