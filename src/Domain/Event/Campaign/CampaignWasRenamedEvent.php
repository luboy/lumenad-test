<?php declare(strict_types=1);

namespace App\Domain\Event\Campaign;

use App\Domain\AggregateRoot\Id\AggregateRootId;
use App\Domain\AggregateRoot\Id\CampaignId;
use App\Domain\Event\Interfaces\EventsourcedEventInterface;

class CampaignWasRenamedEvent implements EventsourcedEventInterface
{
    private CampaignId $campaignId;

    private string $name;

    public function __construct(CampaignId $campaignId, string $name)
    {
        $this->campaignId = $campaignId;
        $this->name = $name;
    }

    public function getAggregateId(): AggregateRootId
    {
        return $this->campaignId;
    }

    public function getCampaignId(): CampaignId
    {
        return $this->campaignId;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
