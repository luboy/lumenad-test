<?php declare(strict_types=1);

namespace App\Domain\Event\Campaign;

use App\Domain\AggregateRoot\Id\AggregateRootId;
use App\Domain\AggregateRoot\Id\CampaignId;
use App\Domain\Event\Interfaces\EventsourcedEventInterface;

class PerformanceRecordWasNotImportedEvent implements EventsourcedEventInterface
{
    // this is used due to DateTime problematic implementation, which break serialization when using reflection
    // brick/date-time would be better suited for date representation
    public const DATE_TO_STRING_FORMAT = 'd-m-Y';

    private CampaignId $campaignId;

    private string $date;

    private string $eventType;

    private int $hits;

    public function __construct(CampaignId $campaignId, \DateTimeImmutable $date, string $eventType, int $hits)
    {
        $this->campaignId = $campaignId;
        $this->date = $date->format(self::DATE_TO_STRING_FORMAT);
        $this->eventType = $eventType;
        $this->hits = $hits;
    }

    public function getAggregateId(): AggregateRootId
    {
        return $this->campaignId;
    }

    public function getCampaignId(): CampaignId
    {
        return $this->campaignId;
    }

    public function getDate(): \DateTimeImmutable
    {
        /** @var \DateTimeImmutable $date */
        $date = \DateTimeImmutable::createFromFormat(self::DATE_TO_STRING_FORMAT, $this->date);

        return $date->setTime(0,0,0,0);
    }

    public function getEventType(): string
    {
        return $this->eventType;
    }

    public function getHits(): int
    {
        return $this->hits;
    }
}
