<?php declare(strict_types=1);

namespace App\Domain\Event\Campaign;

use App\Domain\AggregateRoot\Id\AggregateRootId;
use App\Domain\AggregateRoot\Id\CampaignId;
use App\Domain\Event\Interfaces\EventsourcedEventInterface;

class CampaignWasCreatedEvent implements EventsourcedEventInterface
{
    private CampaignId $campaignId;

    public function __construct(CampaignId $campaignId)
    {
        $this->campaignId = $campaignId;
    }

    public function getAggregateId(): AggregateRootId
    {
        return $this->campaignId;
    }

    public function getCampaignId(): CampaignId
    {
        return $this->campaignId;
    }
}
