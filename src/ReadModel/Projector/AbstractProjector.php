<?php declare(strict_types=1);

namespace App\ReadModel\Projector;

use App\Domain\Event\Interfaces\EventsourcedEventInterface;

class AbstractProjector
{
    public function apply(EventsourcedEventInterface $event): void
    {
        $method = $this->getApplyMethod($event);
        $this->$method($event);
    }

    protected function getApplyMethod(EventsourcedEventInterface $event): string
    {
        $classNameParts = \explode('\\', \get_class($event));

        return 'apply' . \end($classNameParts);
    }
}
