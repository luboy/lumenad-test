<?php declare(strict_types=1);

namespace App\ReadModel\Projector;

use App\Domain\Event\Campaign\CampaignWasCreatedEvent;
use App\Domain\Event\Campaign\CampaignWasRenamedEvent;
use App\Domain\Event\Campaign\PerformanceRecordWasAddedForCampaignEvent;
use App\Domain\Event\Campaign\PerformanceRecordWasNotImportedEvent;
use App\Domain\Event\Campaign\PerformanceRecordWasUpdatedEvent;
use App\ReadModel\Entity\CampaignProjection;
use App\ReadModel\Entity\PerformanceProjection;
use App\ReadModel\Entity\UnImportedCampaignProjection;
use App\ReadModel\Entity\UnImportedPerformanceRecord;
use App\ReadModel\Repository\CampaignProjectionRepositoryInterface;
use App\ReadModel\Repository\PerformanceProjectionRepositoryInterface;
use App\ReadModel\Repository\UnImportedCampaignProjectionRepositoryInterface;
use App\ReadModel\Repository\UnImportedPerformanceRecordRepositoryInterface;

class CampaignProjector extends AbstractProjector
{
    private CampaignProjectionRepositoryInterface $campaignProjectionRepository;

    private PerformanceProjectionRepositoryInterface $performanceProjectionRepository;

    private UnImportedCampaignProjectionRepositoryInterface $unImportedCampaignProjectionRepository;

    private UnImportedPerformanceRecordRepositoryInterface $unImportedPerformanceRecordRepository;

    public function __construct(CampaignProjectionRepositoryInterface $campaignProjectionRepository,
                                PerformanceProjectionRepositoryInterface $performanceProjectionRepository,
                                UnImportedCampaignProjectionRepositoryInterface $unImportedCampaignProjectionRepository,
                                UnImportedPerformanceRecordRepositoryInterface $unImportedPerformanceRecordRepository)
    {
        $this->campaignProjectionRepository = $campaignProjectionRepository;
        $this->performanceProjectionRepository = $performanceProjectionRepository;
        $this->unImportedCampaignProjectionRepository = $unImportedCampaignProjectionRepository;
        $this->unImportedPerformanceRecordRepository = $unImportedPerformanceRecordRepository;
    }

    protected function applyCampaignWasCreatedEvent(CampaignWasCreatedEvent $event): void
    {
        $campaignProjection = $this->campaignProjectionRepository->findById($event->getCampaignId()->getId());

        if (!$campaignProjection) {
            $campaignProjection = CampaignProjection::create($event->getCampaignId());
            $this->campaignProjectionRepository->save($campaignProjection);
        }

        $unImportedCampaign = $this->unImportedCampaignProjectionRepository->findById($event->getCampaignId()->getId());

        if (!$unImportedCampaign) {
            return;
        }

        $this->unImportedCampaignProjectionRepository->delete($unImportedCampaign);
    }

    protected function applyCampaignWasRenamedEvent(CampaignWasRenamedEvent $event): void
    {
        $campaignProjection = $this->campaignProjectionRepository->findById($event->getCampaignId()->getId());

        if (!$campaignProjection) {
            return;
        }

        $campaignProjection->rename($event->getName());
        $this->campaignProjectionRepository->save($campaignProjection);

        $performanceRecords = $this->performanceProjectionRepository->findByCampaign($event->getCampaignId()->getId());

        /** @var PerformanceProjection $performanceRecord */
        foreach ($performanceRecords as $performanceRecord) {
            $performanceRecord->renameCampaign($event->getName());
            $this->performanceProjectionRepository->save($performanceRecord);
        }
    }

    protected function applyPerformanceRecordWasAddedForCampaignEvent(PerformanceRecordWasAddedForCampaignEvent $event): void
    {
        $campaign = $this->campaignProjectionRepository->findById($event->getCampaignId()->getId());

        if (!$campaign) {
            return;
        }

        $performanceProjection = $this->performanceProjectionRepository->findByCampaignDateType(
            $event->getCampaignId(),
            $event->getDate(),
            $event->getEventType(),
        );

        if (!$performanceProjection) {
            $performanceProjection = PerformanceProjection::create(
                $event->getCampaignId(),
                $campaign->getName(),
                $event->getDate(),
                $event->getEventType(),
                $event->getHits(),
            );

            $this->performanceProjectionRepository->save($performanceProjection);
        }

        $toRemoveRecord = $this->unImportedPerformanceRecordRepository->findByCampaignDateType(
            $event->getCampaignId()->getId(),
            $event->getDate(),
            $event->getEventType(),
        );

        if (!$toRemoveRecord) {
            return;
        }

        $this->unImportedPerformanceRecordRepository->delete($toRemoveRecord);
    }

    protected function applyPerformanceRecordWasUpdatedEvent(PerformanceRecordWasUpdatedEvent $event): void
    {
        /** @var PerformanceProjection $performanceProjection */
        $performanceProjection = $this->performanceProjectionRepository->findByCampaignDateType(
            $event->getCampaignId(),
            $event->getDate(),
            $event->getEventType(),
        );
        $performanceProjection->applyHitsChange($event->getDelta());

        $this->performanceProjectionRepository->save($performanceProjection);
    }

    protected function applyPerformanceRecordWasNotImportedEvent(PerformanceRecordWasNotImportedEvent $event): void
    {
        $unImportedCampaign = $this->unImportedCampaignProjectionRepository->findById($event->getCampaignId()->getId());

        if (!$unImportedCampaign) {
            $unImportedCampaign = UnImportedCampaignProjection::create($event->getCampaignId());
            $this->unImportedCampaignProjectionRepository->save($unImportedCampaign);
        }

        $unImportedRecord = $this->unImportedPerformanceRecordRepository->findByCampaignDateType(
            $event->getCampaignId()->getId(),
            $event->getDate(),
            $event->getEventType(),
        );

        if (!$unImportedRecord) {
            $unImportedRecord = UnImportedPerformanceRecord::create(
                $event->getCampaignId(),
                $event->getDate(),
                $event->getEventType(),
                $event->getHits(),
            );
        }

        $this->unImportedPerformanceRecordRepository->save($unImportedRecord);
    }
}
