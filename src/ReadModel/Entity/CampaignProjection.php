<?php declare(strict_types=1);

namespace App\ReadModel\Entity;

use App\Domain\AggregateRoot\Id\CampaignId;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Infrastructure\Repository\Orm\CampaignProjectionRepository")
 */
class CampaignProjection
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=100)
     */
    private string $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private string $name;

    public function __construct(CampaignId $campaignId)
    {
        $this->id = $campaignId->getId();
        $this->name = '';
    }

    public static function create(CampaignId $campaignId): self
    {
        return new self($campaignId);
    }

    // ---------- state changes ----------

    public function rename(string $name): void
    {
        $this->name = $name;
    }

    // ---------- getters ----------

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function toArray(): array
    {
        return [
            'campaignId' => $this->id,
            'name' => $this->name,
        ];
    }
}
