<?php declare(strict_types=1);

namespace App\ReadModel\Entity;

use App\Domain\AggregateRoot\Id\CampaignId;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Infrastructure\Repository\Orm\UnimportedPerformanceRecordRepository")
 */
class UnImportedPerformanceRecord
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private string $campaignId;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private \DateTimeImmutable $date;

    /**
     * @ORM\Column(type="string")
     */
    private string $eventType;

    /**
     * @ORM\Column(type="integer")
     */
    private int $hits = 0;

    public function __construct(CampaignId $campaignId, \DateTimeImmutable $date, string $eventType, int $hits)
    {
        $this->campaignId = $campaignId->getId();
        $this->date = $date->setTime(0,0,0, 0);
        $this->eventType = $eventType;
        $this->hits = $hits;
    }

    public static function create(CampaignId $campaignId, \DateTimeImmutable $date, string $eventType, int $hits): self
    {
        return new self($campaignId, $date, $eventType, $hits);
    }

    // ---------- state changes ----------

    public function setHits(int $hits): void
    {
        $this->hits = $hits;
    }

    // ---------- getters ----------

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCampaignId(): string
    {
        return $this->campaignId;
    }

    public function getDate(): \DateTimeImmutable
    {
        return $this->date;
    }

    public function getEventType(): string
    {
        return $this->eventType;
    }

    public function getHits(): int
    {
        return $this->hits;
    }

    public function toArray(): array
    {
        return [
            'campaignId' => $this->campaignId,
            'date' => $this->date->format('d-m-Y'),
            'event' => $this->eventType,
            'value' => $this->hits,
        ];
    }
}
