<?php declare(strict_types=1);

namespace App\ReadModel\Entity;

use App\Domain\AggregateRoot\Id\CampaignId;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Infrastructure\Repository\Orm\PerformanceProjectionRepository")
 */
class PerformanceProjection
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private string $campaignId;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private string $campaignName;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private \DateTimeImmutable $date;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private string $eventType;

    /**
     * @ORM\Column(type="integer")
     */
    private int $hits = 0;

    public function __construct(CampaignId $campaignId, string $campaignName, \DateTimeImmutable $date, string $eventType, int $hits)
    {
        $this->campaignId = $campaignId->getId();
        $this->campaignName = $campaignName;
        $this->date = $date;
        $this->eventType = $eventType;
        $this->hits = $hits;
    }

    public static function create(CampaignId $campaignId, string $campaignName, \DateTimeImmutable $date, string $eventType, int $hits): self
    {
        return new self($campaignId, $campaignName, $date, $eventType, $hits);
    }

    // ---------- state changes ----------

    public function applyHitsChange(int $delta): void
    {
        $this->hits += $delta;
    }

    public function renameCampaign(string $name): void
    {
        $this->campaignName = $name;
    }

    // ---------- getters ----------

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCampaignId(): string
    {
        return $this->campaignId;
    }

    public function getCampaignName(): string
    {
        return $this->campaignName;
    }

    public function getDate(): \DateTimeImmutable
    {
        return $this->date;
    }

    public function getEventType(): string
    {
        return $this->eventType;
    }

    public function getHits(): int
    {
        return $this->hits;
    }

    public function toArray(): array
    {
        return [
            'date' => $this->date->format('d-m-Y'),
            'event' => $this->eventType,
            'value' => $this->hits,
        ];
    }
}
