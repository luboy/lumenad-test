<?php declare(strict_types=1);

namespace App\ReadModel\Entity;

use App\Domain\Event\Interfaces\EventsourcedEventInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Infrastructure\EventStore\OrmEventStore")
 */
class EventStoreRecord
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=36)
     */
    private string $guid;

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     */
    private int $playhead;

    /**
     * @ORM\Column(type="text")
     */
    private string $serializedEvent;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private \DateTimeImmutable $recorded;

    public function __construct(EventsourcedEventInterface $event, int $playhead, string $serializedEvent)
    {
        $this->guid = (string) $event->getAggregateId();
        $this->playhead = $playhead;
        $this->serializedEvent = $serializedEvent;
        $this->recorded = new \DateTimeImmutable();
    }

    public static function create(EventsourcedEventInterface $event, int $playhead, string $serializedEvent): self
    {
        return new self($event, $playhead, $serializedEvent);
    }

    public function getGuid(): string
    {
        return $this->guid;
    }

    public function getPlayhead(): int
    {
        return $this->playhead;
    }

    public function getSerializedEvent(): string
    {
        return $this->serializedEvent;
    }

    public function getRecorded(): \DateTimeImmutable
    {
        return $this->recorded;
    }
}
