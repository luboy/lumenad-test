<?php declare(strict_types=1);

namespace App\ReadModel\Entity;

use App\Domain\AggregateRoot\Id\CampaignId;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Infrastructure\Repository\Orm\UnImportedCampaignProjectionRepository")
 */
class UnImportedCampaignProjection
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=100)
     */
    private string $id;

    public function __construct(CampaignId $campaignId)
    {
        $this->id = $campaignId->getId();
    }

    public static function create(CampaignId $campaignId): self
    {
        return new self($campaignId);
    }

    // ---------- getters ----------

    public function getId(): string
    {
        return $this->id;
    }

    public function toArray(): array
    {
        return [
            'campaignId' => $this->id,
        ];
    }
}
