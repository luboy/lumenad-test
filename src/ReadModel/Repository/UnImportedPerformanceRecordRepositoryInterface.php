<?php declare(strict_types=1);

namespace App\ReadModel\Repository;

use App\ReadModel\Entity\UnImportedPerformanceRecord;
use Ds\Map;

interface UnImportedPerformanceRecordRepositoryInterface
{
    public function save(UnImportedPerformanceRecord $performanceRecord): void;

    /**
     * @return Map<string, UnImportedPerformanceRecord>
     */
    public function findAllRecords(): Map;

    public function delete(UnImportedPerformanceRecord $unImportedPerformanceRecord): void;

    public function findByCampaignDateType(string $campaignId, \DateTimeImmutable $date, string $eventType): ?UnImportedPerformanceRecord;

    /**
     * @param string $campaignId
     * @return Map<string, UnImportedPerformanceRecord>
     */
    public function findByCampaign(string $campaignId): Map;
}
