<?php declare(strict_types=1);

namespace App\ReadModel\Repository;

use App\ReadModel\Entity\CampaignProjection;
use Ds\Map;

interface CampaignProjectionRepositoryInterface
{
    public function save(CampaignProjection $campaignProjection): void;

    /**
     * @return Map<string, CampaignProjection>
     */
    public function findAllRecords(): Map;

    public function findById(string $id): ?CampaignProjection;
}
