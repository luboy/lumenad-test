<?php declare(strict_types=1);

namespace App\ReadModel\Repository;

use App\Domain\AggregateRoot\Id\CampaignId;
use App\ReadModel\Entity\PerformanceProjection;
use DateTimeImmutable;
use Ds\Map;

interface PerformanceProjectionRepositoryInterface
{
    public function save(PerformanceProjection $campaignProjection): void;

    /**
     * @return Map<string, PerformanceProjection>
     */
    public function findAllRecords(): Map;

    public function findByCampaignDateType(CampaignId $campaignId, DateTimeImmutable $date, string $eventType): ?PerformanceProjection;

    /**
     * @param string $campaignId
     * @return Map<string, PerformanceProjection>
     */
    public function findByCampaign(string $campaignId): Map;
}
