<?php declare(strict_types=1);

namespace App\ReadModel\Repository;

use App\ReadModel\Entity\UnImportedCampaignProjection;
use Ds\Map;

interface UnImportedCampaignProjectionRepositoryInterface
{
    public function save(UnImportedCampaignProjection $unImportedCampaignProjection): void;

    /**
     * @return Map<string, UnImportedCampaignProjection>
     */
    public function findAllRecords(): Map;

    public function findById(string $id): ?UnImportedCampaignProjection;

    public function delete(UnImportedCampaignProjection $unImportedCampaignProjection): void;
}
