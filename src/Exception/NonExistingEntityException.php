<?php declare(strict_types=1);

namespace App\Exception;

use Exception;
use Throwable;

class NonExistingEntityException extends Exception
{
    /**
     * @param string $entityClass
     * @param object $entityIdentifier
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $entityClass, object $entityIdentifier, ?string $message = null, int $code = 404, ?Throwable $previous = null)
    {
        $message = $message ?? \sprintf('Entity "%s:%s" does not exist.', $entityClass, (string) $entityIdentifier);
        parent::__construct($message, $code, $previous);
    }
}
