<?php declare(strict_types=1);

namespace App\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validation;

abstract class AbstractValidator
{
    /**
     * @param array<array<string|int|bool<array<string>>>>  $items
     * @throws \Exception
     */
    protected static function validate(array $items, Constraint $constraint): void
    {
        $validator = Validation::createValidator();

        /** @var ConstraintViolationList<int, ConstraintViolation> $violations */
        $violations = $validator->validate($items, $constraint);

        if (\count($violations) > 0) {
            // FIXME consider other exception type
            throw new \Exception((string) $violations);
        }
    }
}
