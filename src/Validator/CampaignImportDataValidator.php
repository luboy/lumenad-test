<?php declare(strict_types=1);

namespace App\Validator;

use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class CampaignImportDataValidator extends AbstractValidator
{
    /**
     * @param array $campaignData
     * @throws Exception
     */
    public static function validateCampaignData(array $campaignData): void
    {
        $constraint = new Assert\All(['constraints' => [
            new Assert\Collection([
                'campaignId' => [
                   new Assert\NotBlank(),
                   new Assert\Type('string'),
                ],
                'name' => [
                    new Assert\NotBlank(),
                    new Assert\Type('string'),
                ],
            ]),
        ]]);

        self::validate($campaignData, $constraint);
    }

    public static function validateRequest(Request $request): void
    {
        $data = $request->files->all();

        $constraint = new Assert\Collection([
            'file' => [
                new Assert\NotBlank(),
                new Assert\File(['mimeTypes' => ['text/plain']]),
            ],
        ]);

        self::validate($data, $constraint);
    }
}
