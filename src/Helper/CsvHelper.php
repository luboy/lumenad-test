<?php declare(strict_types=1);

namespace App\Helper;

class CsvHelper
{
    public static  function getCsvContent(string $type, string $filepath): array
    {
        $rows = [];

        $handle = \fopen($filepath, 'r');

        if (!$handle) {
            throw new \Exception('File cannot be read.');
        }

        do {
            $data = \fgetcsv($handle, 0, ',');

            if ($data === false) {
                break;
            }

            $rows[] = $data;
        } while ($data !== false);
        \fclose($handle);

        $method = 'process'.\ucfirst($type);

        return self::$method($rows);
    }

    public static function processPerformanceRecord(array $data): array
    {
        return \array_map(static function (array $row) {
            return [
                'campaignId' => (string) $row[0],
                'date' => new \DateTimeImmutable($row[1]),
                'eventType' => (string) $row[2],
                'hits' => \intval($row[3]),
            ];
        }, $data);
    }

    public static function processCampaign(array $data): array
    {
        return \array_map(static function (array $row) {
            return [
                'campaignId' => $row[0],
                'name' => $row[1],
            ];
        }, $data);
    }
}
